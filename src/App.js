import { initGameEngine } from "./GameEngine";
import { initGameObject } from './GameObject';
import { GameEvent } from './GameEvent';
import { initPlayer } from "./Player";

export function App() {
    console.log('INIT APP');
    initGameEngine();
    initGameObject();
    //initPlayer();
    GameEvent();
}