import { Bodies, Composite } from "matter-js";
import { engine } from "./GameEngine";


export const player = Bodies.circle(460, 10, 40, {
    friction: 0, frictionStatic: 0, frictionAir: 0, restitution: 1
});

export function initPlayer() {
    Composite.add(engine.world, [player]);
}