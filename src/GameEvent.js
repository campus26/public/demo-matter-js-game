import { Body, Events } from "matter-js";
import { addGameObject } from "./GameObject";
import { player } from "./Player";
import { engine, initGameEngine } from "./GameEngine";

let cpt = 0;

export function GameEvent() {
    document.addEventListener('click', handlerClick);
    document.addEventListener("keydown", handlerKeyboard);
    Events.on(engine, 'collisionStart', handlerCollisionStart);
}

function handlerClick(event) {
    console.log(event.clientX, event.clientY);
    const x = event.clientX-40;
    const y = event.clientY-40;
    addGameObject(x,y);
}

function handlerKeyboard(event) {
    const keyName = event.key;
    //console.log(keyName);
    switch (keyName) {
        case "ArrowLeft":
            Body.applyForce(player, { x: player.position.x, y: player.position.y }, { x: -0.01, y: 0 });
            console.log(player);
        break;

        case "ArrowRight":
            Body.applyForce(player, { x: player.position.x, y: player.position.y }, { x: 0.01, y: 0 });
            console.log(player);
        break;

        case "ArrowUp":
            Body.applyForce(player, { x: player.position.x, y: player.position.y }, { x: 0, y: -0.02 });
            console.log(player);
        break;
        
        case "ArrowDown":
            Body.applyForce(player, { x: player.position.x, y: player.position.y }, { x: 0, y: 0.02 });
            console.log(player);
        break;
    
        default: break;
    }

}



// Collision detection
function handlerCollisionStart(event) {

    const pairs = event.pairs;
    pairs.forEach(function (pair) {
        if (pair.bodyA.label == 'sol' || pair.bodyB.label == 'sol') {
            //console.log(pair.bodyA.label, 'and', pair.bodyB.label);
            cpt++;
        }
    });
    console.log(cpt);
    if (cpt > 1) {
        window.location.reload();
        alert('GAME OVER')
    }
}
