import { Bodies, Composite } from "matter-js";
import { engine } from "./GameEngine";

export const sol = Bodies.rectangle(400, 700, 710, 60, {
    label: 'sol',
    isStatic: true,
    friction: 0,
    frictionStatic: 0,
    frictionAir: 0,
    restitution: 1
});

export function initGameObject() {
    const boxA = Bodies.rectangle(420, 180, 80, 80, { label: 'boxA', isStatic: true });
    //const ballA = Bodies.circle(380, 100, 40, { friction: 0, frictionStatic: 0, frictionAir: 0, restitution:1 });
    //const ballB = Bodies.circle(460, 10, 40, { friction: 0, frictionStatic: 0, frictionAir: 0, restitution: 1 });
    const plafond = Bodies.rectangle(400, 0, 710, 60, { label: 'plafond',  isStatic: true, friction: 0, frictionStatic: 0, frictionAir: 0, restitution: 1 });
    const murD = Bodies.rectangle(0, 400, 60, 810, { label: 'murD', isStatic: true, friction: 0, frictionStatic: 0, frictionAir: 0, restitution: 1 });
    const murG = Bodies.rectangle(800, 400, 60, 810, { label: 'murG', isStatic: true, friction: 0, frictionStatic: 0, frictionAir: 0, restitution: 1 });

    Composite.add(engine.world, [boxA, sol, plafond, murD, murG]);

}

export function addGameObject(x, y) {
    const box = Bodies.rectangle(x, y, 80, 80, { label: 'box', });
    Composite.add(engine.world, [box]);
}

