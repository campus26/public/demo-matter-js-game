import { Engine, Render, Runner, Bodies, Composite } from "matter-js";


// create an engine
export const engine = Engine.create();
export const runner = Runner.create();

engine.world.gravity.x = 0;
engine.world.gravity.y = 1.5;

// create a renderer
export const render = Render.create({
    element: document.querySelector('#app'),
    engine: engine,
    options: {
        width: 800,
        height: 800,

        wireframes: false,
        background: "white"
    }
});

export function initGameEngine() {
    Render.run(render);
    Runner.run(runner, engine);
    console.log('end INIT GAME');
}